# Makefile for the Obsidian Buildbot Project
#
# https://bitbucket.org/fret/obsidian-buildbot.git
#
# Copyright 2020 Curtis Sand

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# BUILDMASTER and WORKERS variables must be defined and passed in as
# environment variables.
NO_WRAPPER_MSG = "ERROR: Use a wrapper script to operate a buildbot install."
ifndef BUILDMASTER
$(error $(NO_WRAPPER_MSG))
endif

# Required Environment Variables for Wrappers to define
# TITLE =
# BUILDMASTER =
# WORKRS =

# Optional Environment Variables that Wrappers could define if they want
DB_FILE = $(BUILDMASTER)/state.sqlite
CLEAN_PATTERNS = -name http.log \
				 -o -name twistd.log \
				 -o -name twistd.hostname \
				 -o -name master.cfg.sample

# Variables that are most likely going to be common for all installs
PYVENV = $$(pwd)/pyvenv
PYREQS = $$(pwd)/requirements.txt

.PHONY: help
help:
	@echo "$(TITLE)"
	@echo "Targets:"
	@echo "  install : install the required packages to run the buildbot."
	@echo "  clean-db : remove just the database file ($(DB_FILE))."
	@echo "  clean : remove all the generated or temp files in the project."
	@echo "  upgrade-master : run the buildbot upgrade-master command."
	@echo "  start : start the buildbot and the workers."
	@echo "  stop : stop the buildbot and the workers."
	@echo "  restart : restart the buildbot and the workers."

install:
	python -m venv $(PYVENV)
	$(PYVENV)/bin/pip install -U pip
	$(PYVENV)/bin/pip install -r $(PYREQS)
	date > install

.PHONY: clean-db
clean-db:
	rm -f $(DB_FILE) create-db

.PHONY: clean
clean: clean-db
	rm -rf $(PYVENV) install
	find $(BUILDMASTER) $(WORKERS) $(CLEAN_PATTERNS) | xargs rm -rf

.PHONY: create-db
create-db:
	touch $(DB_FILE)

.PHONY: upgrade-master
upgrade-master: create-db
	$(PYVENV)/bin/buildbot upgrade-master $(BUILDMASTER)
	rm $(BUILDMASTER)/master.cfg.sample

.PHONY: start
start: install upgrade-master
	$(PYVENV)/bin/buildbot start $(BUILDMASTER) && \
		for worker in $(WORKERS); do $(PYVENV)/bin/buildbot-worker start $$worker; done

.PHONY: stop
stop:
	$(PYVENV)/bin/buildbot stop $(BUILDMASTER)
	for worker in $(WORKERS); do $(PYVENV)/bin/buildbot-worker stop $$worker; done

.PHONY: restart
restart:
	$(PYVENV)/bin/buildbot restart $(BUILDMASTER)
	for worker in $(WORKERS); do $(PYVENV)/bin/buildbot-worker restart $$worker; done
